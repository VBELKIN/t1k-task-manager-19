package ru.t1k.vbelkin.tm.api.model;

import ru.t1k.vbelkin.tm.api.service.*;

public interface IServiceLocator {
    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

}
