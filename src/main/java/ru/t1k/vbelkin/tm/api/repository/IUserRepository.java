package ru.t1k.vbelkin.tm.api.repository;

import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User findById(String id);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);
}
