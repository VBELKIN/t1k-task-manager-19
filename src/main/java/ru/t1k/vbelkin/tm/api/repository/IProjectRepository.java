package ru.t1k.vbelkin.tm.api.repository;

import ru.t1k.vbelkin.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

    Project create(String name);

}
