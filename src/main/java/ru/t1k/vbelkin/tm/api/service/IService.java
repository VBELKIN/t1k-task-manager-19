package ru.t1k.vbelkin.tm.api.service;

import ru.t1k.vbelkin.tm.api.repository.IRepository;
import ru.t1k.vbelkin.tm.enumerated.Sort;
import ru.t1k.vbelkin.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {
    void clear();

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    M add(M model);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    Integer getSize();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);
}
