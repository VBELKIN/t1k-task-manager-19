package ru.t1k.vbelkin.tm.api.service;

import ru.t1k.vbelkin.tm.model.User;

public interface IAuthService {
    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();
}
