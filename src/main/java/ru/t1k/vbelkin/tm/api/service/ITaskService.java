package ru.t1k.vbelkin.tm.api.service;

import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task create(String name);

    Task create(String name, String description);
}
