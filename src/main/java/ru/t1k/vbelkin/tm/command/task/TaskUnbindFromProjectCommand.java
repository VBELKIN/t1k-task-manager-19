package ru.t1k.vbelkin.tm.command.task;

import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-unbind-from-project";

    public static final String DESCRIPTION = "Unbind task from project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId, taskId);
    }

}
