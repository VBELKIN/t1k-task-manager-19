package ru.t1k.vbelkin.tm.command.system;

import ru.t1k.vbelkin.tm.api.model.ICommand;
import ru.t1k.vbelkin.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    public static final String NAME = "help";

    public static final String ARGUMENT = "-h";

    public static final String DESCRIPTION = "Display list of terminal command.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

}
