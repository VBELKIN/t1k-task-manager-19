package ru.t1k.vbelkin.tm.command.project;

import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-change-status-by-index";

    public static final String DESCRIPTION = "Change project status by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusByIndex(index, status);
    }

}
