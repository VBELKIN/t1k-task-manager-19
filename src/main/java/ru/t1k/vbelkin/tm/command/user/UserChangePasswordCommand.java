package ru.t1k.vbelkin.tm.command.user;

import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    private static final String NAME = "change-password";

    private static final String DESCRIPTION = "change current user password";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        final String id = serviceLocator.getAuthService().getUserId();
        System.out.print("NEW PASSWORD: ");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(id, newPassword);
    }

}
