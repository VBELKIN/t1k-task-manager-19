package ru.t1k.vbelkin.tm.command.user;

import ru.t1k.vbelkin.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    private static final String NAME = "view-user-profile";

    private static final String DESCRIPTION = "view profile of current user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }


}
