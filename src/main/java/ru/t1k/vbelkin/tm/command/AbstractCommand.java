package ru.t1k.vbelkin.tm.command;

import ru.t1k.vbelkin.tm.api.model.ICommand;
import ru.t1k.vbelkin.tm.api.model.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description + "\n";
        return result;
    }

}