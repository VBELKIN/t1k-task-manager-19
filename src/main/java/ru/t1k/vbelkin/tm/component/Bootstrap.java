package ru.t1k.vbelkin.tm.component;

import ru.t1k.vbelkin.tm.api.model.IServiceLocator;
import ru.t1k.vbelkin.tm.api.repository.ICommandRepository;
import ru.t1k.vbelkin.tm.api.repository.IProjectRepository;
import ru.t1k.vbelkin.tm.api.repository.ITaskRepository;
import ru.t1k.vbelkin.tm.api.repository.IUserRepository;
import ru.t1k.vbelkin.tm.api.service.*;
import ru.t1k.vbelkin.tm.command.AbstractCommand;
import ru.t1k.vbelkin.tm.command.project.*;
import ru.t1k.vbelkin.tm.command.system.*;
import ru.t1k.vbelkin.tm.command.task.*;
import ru.t1k.vbelkin.tm.command.user.*;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1k.vbelkin.tm.exception.system.CommandNotSupportedException;
import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.repository.CommandRepository;
import ru.t1k.vbelkin.tm.repository.ProjectRepository;
import ru.t1k.vbelkin.tm.repository.TaskRepository;
import ru.t1k.vbelkin.tm.repository.UserRepository;
import ru.t1k.vbelkin.tm.service.*;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);


    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());

        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserViewProfileCommand());

    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("Test Project", Status.IN_PROGRESS));
        projectService.add(new Project("Demo Project", Status.NOT_STARTED));
        projectService.add(new Project("Beta Project", Status.IN_PROGRESS));
        projectService.add(new Project("Alpha Project", Status.COMPLETED));

        taskService.create("MEGA TASK");
        taskService.create("BETA TASK");
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        try {
            processArgument(arg);
        } catch (final Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    public void run(final String[] args) {
        if (processArguments(args)) System.exit(0);
        initDemoData();
        initLogger();
        processCommands();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }
}
