package ru.t1k.vbelkin.tm.model;

import ru.t1k.vbelkin.tm.api.model.IWBS;
import ru.t1k.vbelkin.tm.enumerated.Status;

import java.util.Date;

public final class Project extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private Date created = new Date();
    private Status status = Status.NOT_STARTED;

    public Project() {
    }

    public Project(String name) {
    }

    public Project(String name, Status status) {
        this.name = name;
        this.status = status;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setName() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
