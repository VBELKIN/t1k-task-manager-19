package ru.t1k.vbelkin.tm.model;

import ru.t1k.vbelkin.tm.api.model.IWBS;
import ru.t1k.vbelkin.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractModel implements IWBS {

    private String name = "";

    private String description = "";

    private String projectId;

    private Status status = Status.NOT_STARTED;
    private Date created = new Date();

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Task() {

    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setName() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return name + " : " + description;
    }

}
